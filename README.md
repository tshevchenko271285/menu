# MENU
Site menu module

## Installation
```
composer require tshevchenko/menu
```

## Migration
Running migrations to create tables in the database
```
php artisan migrate
```
