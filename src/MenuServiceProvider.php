<?php

namespace Tshevchenko\Menu;

use Illuminate\Support\ServiceProvider;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->load();

        $this->publish();
    }

    private function load()
    {
        $this->loadMigrationsFrom(__DIR__.'/Database/migrations');
    }

    private function publish()
    {
//        $this->publishes([
//            __DIR__.'/config/rbac.php' => config_path('rbac.php'),
//        ], 'rbac-config');
    }
}
